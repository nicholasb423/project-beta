import React, {useEffect, useState} from "react";

function SaleForm() {
    const [automobile_vin, setAutomobileVin] = useState('');
    const [automobile_vins, setAutomobileVins] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');


    const fetchData = async () => {
        const automovile_vinUrl = 'http://localhost:8100/api/automobiles/';
        const vin_response = await fetch(automovile_vinUrl);
        if (vin_response.ok) {
          let data = await vin_response.json();
          const autos = data.autos
          var auto_vin = []
          autos.forEach(function(auto) {
            if (auto["sold"] === false) {
                auto_vin.push(auto)
            }
          });
          setAutomobileVins(autos);
        };
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const salesperson_response = await fetch(salespersonUrl);
        if (salesperson_response.ok) {
            let data = await salesperson_response.json();
            setSalespersons(data.salespersons);
        };
        const customerUrl = 'http://localhost:8090/api/customers/';
        const customer_response = await fetch(customerUrl);
        if (customer_response.ok) {
            let data = await customer_response.json();
            setCustomers(data.customers);
        }
    }



    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile_vin;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;



        const saleUrl = 'http://localhost:8090/api/sales/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const saleResponse = await fetch(saleUrl, fetchConfig);
        if (saleResponse.ok) {
            setAutomobileVin('');
            setSalesperson('');
            setCustomer('');
            setPrice('');

        }
    }

    const handleChangeAutomobileVin = (event) => {
        const value = event.target.value;
        setAutomobileVin(value);
    }

    const handleChangeSalesperson = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleChangeCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleChangePrice = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    let dropdownClasses = 'form-select';

    useEffect(() => {
        fetchData()
      }, []);

    return (
        <div className="container">
        <div className="row">
            <div className="col align-self-center">
            </div>
            <div className="col">
            <div className="card shadow">
                <div className="card-body">
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <h1 className="card-title">Add a Salesperson</h1>
                    <div className="mb-3">
                        <select onChange={handleChangeAutomobileVin} name="conference" id="conference" className={dropdownClasses} required>
                            <option>Choose a VIN</option>
                            {automobile_vins.map(automobile_vin => {
                                return (
                                    <option key={automobile_vin.href} value={automobile_vin.vin}>
                                        {automobile_vin.vin}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleChangeSalesperson} name="conference" id="conference" className={dropdownClasses} required>
                            <option>Choose a Salesperson</option>
                            {salespersons.map(salesperson => {
                                return (
                                    <option key={salesperson.href} value={salesperson.employee_id}>
                                        {salesperson.first_name} {salesperson.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleChangeCustomer} name="conference" id="conference" className={dropdownClasses} required>
                            <option>Choose a Customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.href} value={customer.phone_number}>
                                        {customer.first_name} {customer.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangePrice} required placeholder="Price" type="number" id="price" name="price" value={price} className="form-control" />
                        <label>Price</label>
                        </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Create!</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    );
}

export default SaleForm;
