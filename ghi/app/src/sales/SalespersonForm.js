import React, {useState} from "react";

function SalespersonForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;


        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const salespersonResponse = await fetch(salespersonUrl, fetchConfig);
        if (salespersonResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }

    const handleChangeFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleChangeLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleChangeEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }


    return (
        <div className="container">
        <div className="row">
            <div className="col align-self-center">
            </div>
            <div className="col">
            <div className="card shadow">
                <div className="card-body">
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <h1 className="card-title">Add a Salesperson</h1>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeFirstName} required placeholder="First Name" type="text" id="first_name" name="first_name" value={first_name} className="form-control" />
                        <label htmlFor="name">First Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeLastName} required placeholder="Last Name" type="text" id="last_name" name="last_name" value={last_name} className="form-control" />
                        <label htmlFor="email">Last Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeEmployeeId} required placeholder="Employee Id" type="number" id="employee_id" name="employee_id" value={employee_id} className="form-control" />
                        <label htmlFor="email">EmployeeID</label>
                        </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Create!</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    );
}

export default SalespersonForm;
