import React, {useEffect, useState } from 'react';

function SalespersonList() {
    const [salespersons, setSalespersons] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setSalespersons(data.salespersons)
        }
    }
    useEffect(() => {
        fetchData()
      }, []);
    return(
        <table className='table table-stripped'>
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>EmployeeID</th>
                </tr>
            </thead>
            <tbody>
                {salespersons.map(salesperson =>{
                    return (
                        <tr key={salesperson.employee_id}>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                            <td>{ salesperson.employee_id }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default SalespersonList;
