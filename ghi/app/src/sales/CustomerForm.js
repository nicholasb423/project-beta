import React, {useState} from "react";

function CustomerForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhoneNumber] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.address = address;
        data.phone_number = phone_number;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const customerResponse = await fetch(customerUrl, fetchConfig);
        if (customerResponse.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber(' ');
        }
    }

    const handleChangeFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleChangeLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleChangeAddress = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumber = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }


    return (
        <div className="container">
        <div className="row">
            <div className="col align-self-center">
            </div>
            <div className="col">
            <div className="card shadow">
                <div className="card-body">
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <h1 className="card-title">Add a Salesperson</h1>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeFirstName} required placeholder="First Name" type="text" id="first_name" name="first_name" value={first_name} className="form-control" />
                        <label htmlFor="name">First Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeLastName} required placeholder="Last Name" type="text" id="last_name" name="last_name" value={last_name} className="form-control" />
                        <label htmlFor="name">Last Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeAddress} required placeholder="Address" type="text" id="address" name="address" value={address} className="form-control" />
                        <label htmlFor="adress">Address</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input onChange={handlePhoneNumber} required placeholder="Phone Number" type="number" id="phone_number" name="phone_number" value={phone_number} className="form-control" />
                        <label htmlFor="phone_number">Number</label>
                        </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Create!</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    );
}

export default CustomerForm;
