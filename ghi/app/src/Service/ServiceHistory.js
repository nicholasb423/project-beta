import React, { useState, useEffect } from 'react';


function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [searchVin, setSearchVin] = useState('');
    const [vinResult, setVinResult] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    useEffect(() => {
        fetchData()
    }, []);
    return (
        <div>
            <h1>Service History</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.vin}>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.vip }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                                <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                                <td>{ appointment.technician.first_name }{ appointment.technician.last_name }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.status }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
