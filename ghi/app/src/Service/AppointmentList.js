import React, {useState, useEffect } from 'react';


function AppointmentList() {

    const [appointments, setAppointment] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointment(data.appointments);
        }
    }

    const cancel = async (appointment) => {
        const response = await fetch(`http://localhost:8080/api/appointment/${appointment}/cancel`, {
            method: "PUT",
        });
        if (response.ok) {
            window.location.reload()
        }
    };

    const finish = async (appointment) => {
        const response = await fetch(`http://localhost:8080/api/appointment/${appointment}/finish`, {
            method: "PUT",
        });
        if (response.ok) {
            window.location.reload()
        }
    };


    useEffect(() => {
        fetchData()
    }, []);
    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.vin}>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.vip ? "Yes" : "No" }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                                <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                                <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                <td>{ appointment.reason }</td>
                                <td>
                                    <button type="button" onClick={() => cancel(appointment.id)} className="btn btn-danger">Cancel</button>
                                    <button type="button" onClick={() => finish(appointment.id)} className="btn btn-success">Finish</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentList;
