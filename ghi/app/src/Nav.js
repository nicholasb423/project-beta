import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <Link className="nav-link text-white dropdown-toggle" to="technicians" type="button" data-bs-toggle="dropdown" aria-expanded="false">Services</Link>
              <ul className="dropdown-menu">
                <li><Link className="dropdown-item" to="technicians/new">Add a technician</Link></li>
                <li><Link className="dropdown-item" to="technicians/list">Technicians</Link></li>
                <li><Link className="dropdown-item" to="appointments/new">Create a service appointment</Link></li>
                <li><Link className="dropdown-item" to="appointments/list">Service Appointments</Link></li>
                <li><Link className="dropdown-item" to="/serviceHistory">Service History</Link></li>
              </ul>
            </div>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salesperson">Create Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salespersons">Salespeople List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/customer">Create Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/customers">Customers List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/saleform">Saleform</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
