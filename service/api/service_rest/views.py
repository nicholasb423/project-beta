from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import Technician, Appointment
from .encoders import TechnicianEncoder, AppointmentEncoder, AutomobileVO




@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_technician(request, employee_id):
    if request.method == "GET":
        technician = Technician.objects.get(id=employee_id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=employee_id).delete()
        return JsonResponse(
            {"Deleted": count > 0},
        )
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=employee_id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician id invalid"},
                status=400,
            )
        try:
            if AutomobileVO.objects.get(vin=content["vin"]):
                content["vip"] = True
        except AutomobileVO.DoesNotExist:
            content["vip"] = False

        appointment = Appointment.objects.create(**content)
        appointment.status = "created"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["PUT"])
def appointment_finish(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            if appointment.status == "finished":
                return JsonResponse(
                    {"message": "That appointment was already finished"}
                )
            else:
                appointment.status = "finished"
                appointment.save()
                return JsonResponse(
                    {"status": "finished"}
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "That appointment does not exist"},
                status=404,
            )


@require_http_methods(["PUT"])
def appointment_cancel(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            if appointment.status == "cancel":
                return JsonResponse(
                    {"message": "That appointment was already canceled"}
                )
            else:
                appointment.status = "cancel"
                appointment.save()
                return JsonResponse(
                    {"status": "canceled"}
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "That appointment does not exist"},
                status=404,
            )
